FROM ubuntu:16.04

RUN apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
      curl \
      git \
      html2text \
      openjdk-8-jdk \
      libc6-i386 \
      lib32stdc++6 \
      lib32gcc1 \
      lib32ncurses5 \
      lib32z1 \
      unzip \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN ln -s /bin/bash /usr/local/bin/bash

WORKDIR /home/workdir

VOLUME ["/home/workdir"]

CMD ["/bin/bash"]